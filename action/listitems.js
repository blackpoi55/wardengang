const itemList = [
    {
        name: "Blueprint",
        qty: 1,
        price: 160000,
        image: "/items/blueprint.png"
    },
    {
        name: "กล่องขนม",
        qty: 1,
        price: 45000,
        image: "/items/snackbox.png"
    },
    {
        name: "กล่องตีอาวุธ",
        qty: 1,
        price: 50000,
        image: "/items/weaponbox.png"
    },
    {
        name: "เงินแดง",
        qty: 1,
        price: 40,
        image: "/items/redmoney.png"
    },
    {
        name: "เงินเขียว",
        qty: 1,
        price: 1,
        image: "/items/greenmoney.png"
    },
    {
        name: "เพชร",
        qty: 1,
        price: 100000,
        image: "/items/daimon.png"
    },
    {
        name: "น็อต",
        qty: 1,
        price: 100000,
        image: "/items/nuts.png"
    },
    {
        name: "คูปอง",
        qty: 1,
        price: 150,
        image: "/items/coupon.png"
    },
    {
        name: "ปูนดำ",
        qty: 1,
        price: 120000,
        image: "/items/blackcement.png"
    },
    {
        name: "ปูน",
        qty: 1,
        price: 3500,
        image: "/items/cement.png"
    },
    {
        name: "สายไฟใหญ่",
        qty: 1,
        price: 25000,
        image: "/items/bigwire.png"
    },
    {
        name: "เศษสายไฟ",
        qty: 1,
        price: 2500,
        image: "/items/smallwire.png"
    },
]
export const getitemList = () => {
    return itemList
}
const itemList = [
    {
        name: "Pain",
        pricefamily: 190000,
        pricetier1: 180000,
        pricetier2: 170000,
        image: "/fightitems/pain.png"
    },
    {
        name: "Armor",
        pricefamily: 285000,
        pricetier1: 270000,
        pricetier2: 255000,
        image: "/fightitems/armor.png"
    },
    {
        name: "AED",
        pricefamily: 475000,
        pricetier1: 450000,
        pricetier2: 425000,
        image: "/fightitems/aed.png"
    },
]
export const fightitems = () => {
    return itemList
}
import { getitemList } from '@/action/listitems';
import Nav from '@/components/Nav';
import { useEffect, useState } from 'react';

function calculator() {
    const [listItems, setlistItems] = useState([])
    const [ItemsChoose, setItemsChoose] = useState([])
    const [total, settotal] = useState(0)
    const [setofall, setsetofall] = useState(1)
    useEffect(() => {
        refresh()
    }, [])
    useEffect(() => {
        calculateprice()
    }, [ItemsChoose, setofall])

    const refresh = () => {
        let data = getitemList()
        console.log(data)
        setlistItems(data)
    }
    const itemsClick = (item) => {
        let arr = [...ItemsChoose]
        if (!itemcheck(item)) {
            arr.push(item)
        }
        else {
            arr = arr.filter(x => x.name !== item.name);
        }
        setItemsChoose([...arr])
    }
    const itemcheck = (item) => {
        var check = ItemsChoose.filter((x) => x.name == item.name);
        if (check && check.length > 0) {
            return true
        }
        else {
            return false
        }
    }
    const calculateprice = () => {
        if (ItemsChoose && ItemsChoose.length > 0) {
            let sum = 0
            for (const iterator of ItemsChoose) {
                sum += (iterator.price * (iterator.qty * setofall))
            }
            settotal(sum)
        }
    }
    const handleitemschooseChange = (name, e, index) => {
        ItemsChoose[index][name] = e.target.value
        setItemsChoose([...ItemsChoose])
    }
    const T1Click = () => {
        itemchoosesetdata(1, 1000, 1, 1, 30)
    }
    const T2Click = () => {
        itemchoosesetdata(1, 2000, 2, 2, 30)
    }
    const T2UPClick = () => {
        itemchoosesetdata(1, 2500, 2, 2, 50)
    }
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
    const itemchoosesetdata = (blue, red, snack, weapon, coupon) => {
        let sumitem = []
        for (const iterator of listItems) {
            if (iterator.name == "Blueprint") {
                sumitem.push({ ...iterator, qty: blue })
            }
            else if (iterator.name == "กล่องขนม") {
                sumitem.push({ ...iterator, qty: snack })
            }
            else if (iterator.name == "กล่องตีอาวุธ") {
                sumitem.push({ ...iterator, qty: weapon })
            }
            else if (iterator.name == "เงินแดง") {
                sumitem.push({ ...iterator, qty: red })
            }
            else if (iterator.name == "คูปอง") {
                sumitem.push({ ...iterator, qty: coupon })
            }

        }
        setItemsChoose(sumitem)
        console.log(sumitem)
    }


    return (
        <div className="h-screen bg-gray-900">
            <div className="">
                <Nav></Nav>
                <div>
                    <div>
                    <h1 className="text-red-500 title-font mb-2 text-4xl font-extrabold leading-10 tracking-tight sm:text-5xl sm:leading-none md:text-6xl w-full text-center">Warden</h1>
                        <div className='w-full flex justify-center items-center'>
                            <label>คำนวณราคาชุดตีอาวุธ</label>
                            <button onClick={() => T1Click()} className='p-2 rounded-lg border border-red-500 mx-2'>T1</button>
                            <button onClick={() => T2Click()} className='p-2 rounded-lg border border-red-500 mx-2'>T2</button>
                            <button onClick={() => T2UPClick()} className='p-2 rounded-lg border border-red-500 mx-2'>T2 UP</button>
                            <label className='mx-2'>จำนวน</label>
                            <input className='mx-2 text-center p-2 rounded-lg border' type="number" value={setofall} onChange={(e) => setsetofall(e.target.value)} />
                            <label className='mx-2'>ชุด</label>

                        </div>
                        <div className='w-full m-2 p-2 rounded-2xl h-24 flex justify-center items-center overflow-x-auto'>
                            {listItems.map((p, index) => (
                                <>
                                    <button onClick={() => itemsClick(p)} className={`mr-2 w-96 p-2 rounded-lg text-white  ${itemcheck(p) ? " bg-red-800 " : " bg-gray-800 "}`}>
                                        <div className='flex flex-col justify-center items-center'>
                                            <img src={p.image} alt="" />
                                            <label>{p.name}</label>
                                        </div>
                                    </button>
                                </>
                            ))}
                        </div>
                    </div>
                    <div className='flex justify-center items-center'>
                        <div className="overflow-auto  w-2/3 h-[60vh]">
                            <table className="table table-compact table-auto w-full overflow-scroll bg-[#610000] text-white p-3">
                                <thead className=' rounded-t-2xl shadow-lg bg-[#610000]  sticky top-0 z-50'>
                                    <tr className='text-left border-b border-black'>
                                        <th className="p-2 text-center "></th>
                                        <th className="p-2 text-center ">item</th>
                                        <th className="p-2 text-center ">จำนวน/ครั้ง</th>
                                        <th className="p-2 text-center ">ราคา/ชิ้น</th>
                                        <th className="p-2 text-center ">จำนวน/ชุด</th>
                                        <th className="p-2 text-center ">รวม/ชิ้น</th>
                                        <th className="p-2 text-center ">รวมเป็นเงิน</th>
                                    </tr>
                                </thead>
                                <tbody className='bg-white text-black'>
                                    {ItemsChoose.map((p, index) => (
                                        <tr className={`" cursor-pointer border-b bg-gray-600 text-white hover:bg-[#942d2d] "   `} >
                                            <td className="p-2 text-center"><img src={p.image} alt="" /></td>
                                            <td className="p-2 text-center">{p.name}</td>
                                            <td className="p-2 text-center"><input type="number" className='p-2 rounded-lg border text-center' value={p.qty} onChange={(e) => handleitemschooseChange("qty", e, index)} /></td>
                                            <td className="p-2 text-center"><input type="number" className='p-2 rounded-lg border text-center' value={p.price} onChange={(e) => handleitemschooseChange("price", e, index)} /></td>
                                            <td className="p-2 text-end">{numberWithCommas(setofall)}</td>
                                            <td className="p-2 text-end">{numberWithCommas(setofall * p.qty)}</td>
                                            <td className="p-2 text-end text-green-500">{numberWithCommas((setofall * p.qty) * p.price)}</td>
                                        </tr>
                                    ))}
                                    <tr className={`" text-green-400 text-2xl font-bold cursor-pointer border-b bg-gray-600  hover:bg-[#942d2d] "   `} >
                                        <td colSpan={2} className="p-2 text-center">รวมเป็นเงิน</td>
                                        <td colSpan={3} className="p-2 text-center">{numberWithCommas(total)}</td>
                                        <td colSpan={2} className="p-2 text-center">Saimai Dollar</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer bg-gray-900 relative pt-1 h-1/10">
                <div className="container mx-auto px-6">
                    <div className=" border-t-2 border-red-600 flex flex-col items-center">
                        <div className="sm:w-2/3 text-center py-4">
                            <p className="text-sm text-white font-bold mb-2">
                                Pussy Hunter
                            </p>
                            <p className="text-sm text-white font-bold mb-2">
                                © 2024 by BoatMousay
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default calculator


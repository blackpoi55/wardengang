// pages/_app.js
import { SessionProvider } from 'next-auth/react';
import '../styles/globals.css'; // ถ้ามีไฟล์ CSS ใดๆ ที่ต้องการใช้งาน

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
  return (
    <SessionProvider session={session}>
      <Component {...pageProps} />
    </SessionProvider>
  );
}

export default MyApp;

// pages/member.js
import { useEffect, useState } from 'react';
import { getSheetData } from '../lib/googleSheets';
import Nav from '@/components/Nav';

export default function Home({ sheetData }) {
    const [Header, setHeader] = useState([])
    const [Body, setBody] = useState([])
    useEffect(() => {
        console.log(sheetData)
        if (sheetData?.[0]) {
            setHeader(sheetData[0])
            let arr = []
            for (let index = 1; index < sheetData.length; index++) {
                if (sheetData?.[index]) {
                    arr.push(sheetData[index])
                }
            }
            setBody(arr)
        }
    }, [sheetData])
    const weaponCheck = (weapon) => {
        if (weapon == "ไม้2 up" || weapon == "มีด2 UP" || weapon == "สนับ2 up") {
            return " text-white bg-[#bc3bdd] "
        }
        else if (weapon == "ไม้2" || weapon == "มีด2" || weapon == "สนับ2") {
            return " text-white bg-[#2a8eb0] "
        }
        else if (weapon == "ไม้1" || weapon == "มีด1" || weapon == "สนับ1") {
            return " text-white bg-[#d1dc33] "
        }
        else if (weapon == "มาเช" || weapon == "ปืน") {
            return " text-white bg-red-600 "
        }
        else if (weapon == "ไม้newbie") {
            return " text-white bg-purple-600 "
        }
        else {
            return " text-black "
        }

    }

    return (
        <div className="h-screen bg-gray-900">
            <div className="">
                <Nav></Nav>
                <div>
                    <div className="overflow-auto  w-full h-full">
                        <table className="table table-compact table-auto w-full overflow-scroll bg-[#610000] text-white">
                            <thead className=' rounded-t-2xl shadow-lg bg-[#610000]  sticky top-0 z-50'>
                                <tr className='text-left border-b border-black'>
                                    {Header && Header.length > 0 && Header.map((p, index) => (
                                        <th className="p-2">{p}</th>
                                    ))}
                                </tr>
                            </thead>
                            <tbody className='bg-white text-black'>
                                {Body && Body.length > 0 && Body.map((p, index) => (
                                    <tr className={`" cursor-pointer border-b bg-gray-600 text-white hover:bg-[#942d2d] "   `} >
                                        <td className="p-2 bg-[#610000]  text-center">{p?.[0]}</td>
                                        <td className="p-2   text-center">{p?.[1]}</td>
                                        <td onClick={()=>{(p?.[2])=="101010"?window.open("https://docs.google.com/spreadsheets/d/15cEjZ-MGQU_heMGlTDL08mdNiOVQSrd3JcxTNdgMpuQ/edit?gid=0#gid=0"):""}} className="p-2  text-green-400 ">{p?.[2]}</td>
                                        <td className={`p-2 ${weaponCheck(p?.[3])}`}>{p?.[3]}</td>
                                        <td className={`p-2 ${weaponCheck(p?.[4])}`}>{p?.[4]}</td>
                                        <td className={`p-2 ${weaponCheck(p?.[5])}`}>{p?.[5]}</td>
                                        <td className={`p-2 ${weaponCheck(p?.[6])}`}>{p?.[6]}</td>
                                        <td className={`p-2 ${weaponCheck(p?.[7])}`}>{p?.[7]}</td>
                                        <td className={`p-2   text-center`}>{p?.[8]}</td>
                                        <td className="p-2">{p?.[9]}</td>
                                        <td className="p-2">{p?.[10]}</td>
                                        <td className="p-2">{p?.[11]}</td>
                                        <td className="p-2">{p?.[12]}</td>
                                        <td className="p-2">{p?.[13]}</td>
                                        <td className="p-2">{p?.[14]}</td>
                                        <td className="p-2">{p?.[15]}</td>

                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <footer className="footer bg-gray-900 relative pt-1 h-1/10">
                <div className="container mx-auto px-6">
                    <div className=" border-t-2 border-red-600 flex flex-col items-center">
                        <div className="sm:w-2/3 text-center py-4">
                            <p className="text-sm text-white font-bold mb-2">
                                Pussy Hunter
                            </p>
                            <p className="text-sm text-white font-bold mb-2">
                                © 2024 by BoatMousay
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export async function getServerSideProps() {
    let sheetData = await getSheetData();
    console.log(sheetData)

    // ถ้า sheetData เป็น undefined หรือ null, ตั้งค่าให้เป็น array ว่างๆ
    if (!sheetData) {
        sheetData = [];
    }

    return {
        props: {
            sheetData,
        },
    };
}

import { useEffect, useState } from 'react'
import { getweek, putweek } from '@/action/api'
import Tableworknomal from '@/components/Tableworknomal'
import Nav from '@/components/Nav'



export default function Home() {


  return (
    <div className="h-screen bg-gray-900">
      <div className="">
        <Nav></Nav>
        <div>
          <Tableworknomal></Tableworknomal>
        </div>
      </div>
      <footer className="footer bg-gray-900 relative pt-1 h-1/10">
        <div className="container mx-auto px-6">
          <div className=" border-t-2 border-red-600 flex flex-col items-center">
            <div className="sm:w-2/3 text-center py-4">
              <p className="text-sm text-white font-bold mb-2">
                Pussy Hunter
              </p>
              <p className="text-sm text-white font-bold mb-2">
                © 2024 by BoatMousay
              </p>
            </div>
          </div>
        </div>
      </footer>
    </div>

  )
}

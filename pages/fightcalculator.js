import { fightitems } from '@/action/fightitems';
import Nav from '@/components/Nav';
import { useEffect, useState } from 'react';

function fightcalculator() {
    const [listItems, setlistItems] = useState([])
    const [total, settotal] = useState(0)
    const [qtyitems, setqtyitems] = useState({})
    useEffect(() => {
        refresh()
    }, [])

    const refresh = () => {
        let data = fightitems()
        console.log(data)
        setlistItems(data)
        setqtyitems({
            Pain: 0,
            Armor: 0,
            AED: 0
        })
    }


    const handleqtyitemsChange = (name, e) => {
        qtyitems[name] = e.target.value
        setqtyitems({ ...qtyitems })
    }
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
    const calsum = (nameprice) => {
        let sum = (listItems[0]?.[nameprice] * qtyitems.Pain) + (listItems[1]?.[nameprice] * qtyitems.Armor) + (listItems[2]?.[nameprice] * qtyitems.AED)
        return sum
    }





    return (
        <div className="h-screen bg-gray-900">
            <div className="">
                <Nav></Nav>
                <div>
                    <div>
                    <h1 className="text-red-500 title-font mb-2 text-4xl font-extrabold leading-10 tracking-tight sm:text-5xl sm:leading-none md:text-6xl w-full text-center">Warden</h1>
                        <div className='w-full flex justify-center items-center'>
                            <label>คำนวณราคาสวัสดิการ</label>
                        </div>
                    </div>
                    <div className='flex justify-center items-center'>
                        <div className="overflow-auto  w-4/5 h-[60vh]">
                            <table className="table table-compact table-auto w-full overflow-scroll bg-[#610000] text-white p-3">
                                <thead className=' rounded-t-2xl shadow-lg bg-[#610000]  sticky top-0 z-50'>
                                    <tr className='text-left border-b border-black'>
                                        <th className="p-2 text-center "></th>
                                        <th className="p-2 text-center ">item</th>
                                        <th className="p-2 text-end ">Family</th>
                                        <th className="p-2 text-end ">Tier 1</th>
                                        <th className="p-2 text-end ">Tier 2</th>
                                        <th className="p-2 text-center ">จำนวน/กล่อง</th>
                                        <th className="p-2 text-end ">Family  รวมเป็นเงิน</th>
                                        <th className="p-2 text-end ">Tier 1 รวมเป็นเงิน</th>
                                        <th className="p-2 text-end ">Tier 2 รวมเป็นเงิน</th>
                                    </tr>
                                </thead>
                                <tbody className='bg-white text-black'>
                                    {listItems.map((p, index) => (
                                        <tr className={`" cursor-pointer border-b bg-gray-600 text-white hover:bg-[#942d2d] "   `} >
                                            <td className="p-2 text-center"><img src={p.image} alt="" /></td>
                                            <td className="p-2 text-center">{p.name}</td>
                                            <td className="p-2 text-end text-yellow-400 font-bold">{numberWithCommas(p.pricefamily)}</td>
                                            <td className="p-2 text-end text-orange-400 font-bold">{numberWithCommas(p.pricetier1)}</td>
                                            <td className="p-2 text-end text-red-400 font-bold">{numberWithCommas(p.pricetier2)}</td>
                                            <td className="p-2 text-center"><input type="number" className='p-2 rounded-lg border text-center' value={qtyitems[p.name]} onChange={(e) => handleqtyitemsChange(p.name, e)} /></td>
                                            <td className="p-2 text-end text-yellow-400">{numberWithCommas((qtyitems[p.name]) * p.pricefamily)}</td>
                                            <td className="p-2 text-end text-orange-400">{numberWithCommas((qtyitems[p.name]) * p.pricetier1)}</td>
                                            <td className="p-2 text-end text-red-400">{numberWithCommas((qtyitems[p.name]) * p.pricetier2)}</td>
                                        </tr>
                                    ))}
                                    <tr className={`" text-green-400 text-2xl font-bold cursor-pointer border-b bg-gray-600  hover:bg-[#942d2d] "   `} >
                                        <td colSpan={6} className="p-2 text-center">รวมเป็นเงิน</td>
                                        <td className="p-2 text-center text-yellow-400 underline-offset-4 underline">{numberWithCommas(calsum("pricefamily"))}</td>
                                        <td className="p-2 text-center text-orange-400 underline-offset-4 underline">{numberWithCommas(calsum("pricetier1"))}</td>
                                        <td className="p-2 text-center text-red-400 underline-offset-4 underline">{numberWithCommas(calsum("pricetier2"))}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <footer className="footer bg-gray-900 relative pt-1 h-1/10">
                <div className="container mx-auto px-6">
                    <div className=" border-t-2 border-red-600 flex flex-col items-center">
                        <div className="sm:w-2/3 text-center py-4">
                            <p className="text-sm text-white font-bold mb-2">
                                Pussy Hunter
                            </p>
                            <p className="text-sm text-white font-bold mb-2">
                                © 2024 by BoatMousay
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}

export default fightcalculator


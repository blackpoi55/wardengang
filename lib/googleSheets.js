// import { google } from 'googleapis';

// export async function getSheetData(token) {
//     const auth = new google.auth.OAuth2(
//         process.env.GOOGLE_CLIENT_ID,
//         process.env.GOOGLE_CLIENT_SECRET
//     );
//     auth.setCredentials({ access_token: token });

//     const sheets = google.sheets({ version: 'v4', auth });
//     const response = await sheets.spreadsheets.values.get({
//         spreadsheetId: process.env.GOOGLE_SHEETS_SPREADSHEET_ID,
//         range: 'Sheet1!A1:D10',  // ปรับ range ตามต้องการ
//     });

//     return response.data.values;
// }
import fetch from 'node-fetch';

export async function getSheetData() {
    const spreadsheetId = process.env.GOOGLE_SHEETS_SPREADSHEET_ID;
    const apiKey = process.env.GOOGLE_API_KEY;
    const range = 'member!A2:P42';  // ปรับ range ตามต้องการ

    const url = `https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}/values/${range}?key=${apiKey}`;

    const response = await fetch(url);
    const data = await response.json();

    return data.values;
    // return url
}
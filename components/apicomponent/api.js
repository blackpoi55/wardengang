import axios from 'axios';
import { API } from '../../config';

export const GET = (URL) => {
  const Token = localStorage.getItem("Token")
  // const Token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InR1YSIsInN1YiI6IjY1NDIwZDJhNjViMTA3NDY5NDJiMDM5ZSIsInJvbGVzIjpbInVzZXIiLCJhZG1pbiJdLCJjb2RlIjoiVTAwMDAxIiwiaWF0IjoxNzA0NDI1MjM1LCJleHAiOjE3MTMwNjUyMzV9.GOryS1X-hTAhA8qa2pOtnXIp1JrMiu7TVrB-Rm2j2xA'
  return axios({
    method: 'GET',
    url: `${API}${URL}`,
    headers: {
      'Authorization': 'Bearer ' + Token,
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    // console.log(URL, err);
    return { error: err }
  });

}
export const POST = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'POST',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Authorization': 'Bearer ' + Token,
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    // console.log("token",Token)
    return result.data

  }).catch(err => {
    console.log(err);
    return { error: err }
  });
}
export const POST2 = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'POST',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Authorization': 'Bearer ' + Token,
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data.data

  }).catch(err => {
    console.log(err);
    return { error: err }
  });
}
export const POST_CheckToken = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'POST',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Authorization': Token ? ('Bearer ' + Token) : '',
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    return { error: err }
  });
}
export const PUT = (URL, data) => {
  const Token = "$2a$10$f/nZ8OeacpwMS52q2YuyiuIBhrHUBA9c732vsPGQnTlMdqs1shDb2"
  return axios({
    method: 'PUT',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'X-Master-Key': Token,
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    return { error: err }
  });
}
export const DELETE = (URL) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'DELETE',
    url: `${API}${URL}`,
    // data: data,
    headers: {
      'Authorization': 'Bearer ' + Token,
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    return { error: err }
  });
}
export const DELETE_DATA = (URL,data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'DELETE',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Authorization': 'Bearer ' + Token,
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    return { error: err }
  });
}
export const POSTnoAUTH = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'POST',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    // console.log(err.response.status ==401)
    return { error: err }
  });
}
export const GETnoAUTH = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'GET',
    url: `${API}${URL}`,
    headers: {
      'Content-Type': 'application/json'
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    // console.log(err.response.status ==401)
    return { error: err }
  });
}

export const uploadFileFormData = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'POST',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Content-Type': "multipart/form-data"
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    // console.log(err.response.status ==401)
    return { error: err }
  });
}
export const UploadWithToken = (URL, data) => {
  const Token = localStorage.getItem("Token")
  return axios({
    method: 'POST',
    url: `${API}${URL}`,
    data: data,
    headers: {
      'Authorization': 'Bearer ' + Token,
      'Content-Type': "multipart/form-data"
    },
  }).then((result) => {
    // console.log('getCookie', result.data)
    return result.data

  }).catch(err => {
    console.log(err);
    // console.log(err.response.status ==401)
    return { error: err }
  });
}
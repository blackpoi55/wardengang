import React from 'react'

function Tableworking(props) {
    const { datetimetocheck } = props
    return (
        <div className='w-full h-full flex justify-center items-center'>
            <div className='w-[100%] h-[70vh] border-2 border-red-500 flex flex-col'>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-white flex justify-center items-center `}>วัน</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>20:00 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>20:30 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>21:00 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>21:30 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>22:00 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>22:30 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>23:00 น.</div>
                    <div className='w-[10%] h-full border border-white flex justify-center items-center '>23:30 น.</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-yellow-500 flex justify-center items-center ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time < "2000" ? " bg-blue-600 " : ""}`}>จันทร์</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>ทอง</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>ทอง</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>ทอง</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>ทอง</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>ทอง</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>ทอง</div>
                    <div className={`w-[10%] h-full border border-yellow-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'จันทร์' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-pink-500 flex justify-center items-center ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time < "2000" ? " bg-blue-600 " : ""}`}>อังคาร</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-pink-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อังคาร' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-green-500 flex justify-center items-center ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time < "2000" ? " bg-blue-600 animate-bounce " : ""}`}>พุธ</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>เหล็ก</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>เหล็ก</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>เหล็ก</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>เหล็ก</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>เหล็ก</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>เหล็ก</div>
                    <div className={`w-[10%] h-full border border-green-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'พุธ' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-orange-500 flex justify-center items-center ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time < "2000" ? " bg-blue-600 " : ""}`}>พฤหัสบดี</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-orange-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'พฤหัสบดี' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-blue-500 flex justify-center items-center ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time < "2000" ? " bg-blue-600 " : ""}`}>ศุกร์</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>ไม้</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>ไม้</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>ไม้</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>ไม้</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>ไม้</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-green-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>ไม้</div>
                    <div className={`w-[10%] h-full border border-blue-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'ศุกร์' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-purple-500 flex justify-center items-center ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time < "2000" ? " bg-blue-600 " : ""}`}>เสาร์</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-red-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>งานดำ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-red-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>งานดำ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-red-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>งานดำ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-red-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>งานดำ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-red-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>งานดำ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-red-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>งานดำ</div>
                    <div className={`w-[10%] h-full border border-purple-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'เสาร์' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>เช็คชื่อ</div>
                </div>
                <div className='flex w-full h-[12.5%]'>
                    <div className={`w-[20%] h-full border border-red-500 flex justify-center items-center ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time < "2000" ? " bg-blue-600 " : ""}`}>อาทิตย์</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2000" && datetimetocheck?.time < "2030" ? " bg-blue-600 " : ""}`}>ตีบวก</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2030" && datetimetocheck?.time < "2100" ? " bg-blue-600 " : ""}`}>ตีบวก</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-pink-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2100" && datetimetocheck?.time < "2130" ? " bg-blue-600 " : ""}`}>ตีบวก</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2130" && datetimetocheck?.time < "2200" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2200" && datetimetocheck?.time < "2230" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2230" && datetimetocheck?.time < "2300" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2300" && datetimetocheck?.time < "2330" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                    <div className={`w-[10%] h-full border border-red-500 flex justify-center items-center text-blue-400 ${datetimetocheck?.date == 'อาทิตย์' && datetimetocheck?.time >= "2330" && datetimetocheck?.time < "2359" ? " bg-blue-600 " : ""}`}>ฟาร์มตามใจ</div>
                </div>
            </div>
        </div>
    )
}

export default Tableworking
import { useRouter } from 'next/router'
import React from 'react'

function Nav() {
    const router = useRouter()
    console.log("router", router.asPath)
    return (
        <div className="flex flex-row">
            <div className="justify-center p-1 w-full">
                <button onClick={() => router.push('/')} className={`border border-red-800 ${router.asPath == "/" ? " text-red-600 " : " text-gray-600 "}  p-3 rounded-xl rounded-r-none`}>
                    ตารางงาน
                </button>
                {/* <button onClick={() => router.push('/member')} className={`border border-red-800 ${router.asPath == "/member" ? " text-red-600 " : " text-gray-600 "}  p-3 `}>
                    สมาชิก
                </button> */}
                <button onClick={() => router.push('/fightcalculator')} className={`border border-red-800 ${router.asPath == "/fightcalculator" ? " text-red-600 " : " text-gray-600 "}  p-3 `}>
                    ราคาสวัสดิการ
                </button>
                <button onClick={() => router.push('/calculator')} className={`border border-red-800 ${router.asPath == "/calculator" ? " text-red-600 " : " text-gray-600 "}  p-3 rounded-xl rounded-l-none`}>
                    คำนวณราคาชุดตีอาวุธ
                </button>

            </div>

        </div>
    )
}

export default Nav
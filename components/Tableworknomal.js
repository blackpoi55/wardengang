import { useEffect, useState } from 'react'
import { getweek, putweek } from '@/action/api'
import Tableworking from './Tableworking';
function Tableworknomal() {
    const [currentTime, setCurrentTime] = useState(new Date());
    const [isClient, setIsClient] = useState(false);
    const [datetimetocheck, setdatetimetocheck] = useState({ date: "", time: "" });
  
    useEffect(() => {
      setIsClient(true);
      const intervalId = setInterval(() => {
        setCurrentTime(new Date());
        checkDateTimenow(new Date())
      }, 1000);
  
      return () => clearInterval(intervalId);
    }, []);
  
    const formatTime = (date) => {
      return date.toLocaleTimeString('en-GB', {
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
      });
    };
    function formatTimeToHHMM(date) {
      let hours = date.getHours().toString().padStart(2, '0');
      let minutes = date.getMinutes().toString().padStart(2, '0');
      return hours + minutes;
    }
    function getThaiDayOfWeek(date) {
      const daysOfWeek = [
        'อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัสบดี', 'ศุกร์', 'เสาร์'
      ];
      return daysOfWeek[date.getDay()];
    }
    const checkDateTimenow = (date) => {
      let dayOfWeek = getThaiDayOfWeek(date);
      let timenow = formatTimeToHHMM(date);
      setdatetimetocheck({ date: dayOfWeek, time: timenow })
      console.log(dayOfWeek, timenow);
    }
    return (
        <section className=" text-gray-200 bg-gray-900">
            <div className="max-w-6xl mx-auto px-5 pt-3">
                <div className="flex flex-wrap w-full mb-1 flex-col items-center text-center">
                    <h1 className="text-red-500 title-font mb-2 text-4xl font-extrabold leading-10 tracking-tight text-left sm:text-5xl sm:leading-none md:text-6xl">Warden</h1>
                    <p className="lg:w-1/2 w-full leading-relaxed text-base text-white">
                        {isClient ? "วัน" + datetimetocheck.date + " " + formatTime(currentTime) : "Loading..."}
                    </p>
                    <div className='flex w-full justify-center'>
                        <label className='text-pink-400 mr-2'>เช็คชื่อ</label>
                        <label className='text-green-400 mr-2'>ฟาร์มเข้าตัวเอง</label>
                        <label className='text-red-400 mr-2'>ฟาร์มเข้าแก๊ง</label>
                        <label className='text-blue-400 mr-2'>ฟรี</label>
                    </div>
                </div>
                <Tableworking></Tableworking>
            </div>
        </section>
    )
}

export default Tableworknomal